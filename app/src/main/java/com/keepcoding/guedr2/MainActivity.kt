package com.keepcoding.guedr2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        val TAG = MainActivity::class.java.canonicalName
        val MSG = "He pasado por onCreate"
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.v( TAG, MSG)
    }
}
